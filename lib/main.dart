import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('ListView Flutter v1.0 '),
        ),
        body: ListView( 
          children: [
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
            ListTile(
              title: Text('Arquitectura y diseño de aplicaciones moviles'),
              subtitle: Text('Tecnologico de Pto. Vallarta'),
              leading: CircleAvatar(
                backgroundImage: NetworkImage('https://images.unsplash.com/photo-1644982647711-9129d2ed7ceb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80')
               ),
               trailing: 
               Icon(Icons.mobile_friendly_outlined),
               iconColor: Colors.blue,
            ),
          ],
          padding: EdgeInsets.all(20),
          shrinkWrap: true,
          reverse: true,
          itemExtent: 90,
          scrollDirection: Axis.vertical,
        ),
      ),
    );
  }
}

